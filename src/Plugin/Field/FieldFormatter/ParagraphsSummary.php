<?php

namespace Drupal\paragraphs_summary\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsFormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;

/**
 * Plugin implementation of the 'paragraphs_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraphs_summary",
 *   label = @Translation("Paragraphs Summary"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphsSummary extends EntityReferenceRevisionsFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'allowed_bundles' => '',
      'view_mode' => 'full',
      'trim_length' => '600',
      'filter_format' => 'plain_text',
      'limit' => 1,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $paragraph_bundles = $this->getParagraphBundles();

    $form['allowed_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed bundles'),
      '#options' => $paragraph_bundles,
      '#description' => t('The Paragraph bundles that can be included in the summary.'),
      '#default_value' => $this->getSetting('allowed_bundles'),
    ];

    $form['view_mode'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('View mode'),
      '#description' => t('The Paragraph view mode that should be used.'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
    ];

    $form['filter_format'] = [
      '#type' => 'select',
      '#options' => [$this->t('-- Text Format --')] + $this->getFilterFormats(),
      '#title' => $this->t('Text format'),
      '#default_value' => $this->getSetting('filter_format'),
      '#description' => $this->t('It is strongly recommended to use a <a href="@text-format">text format</a> that uses the limit allowed HTML tags filter to exclude divs from the text. Otherwise, wrapping divs and other HTML will count toward the character limit.',
        ['@text-format' =>Url::fromRoute('filter.admin_overview')->toString()]),
      '#required' => TRUE,
    ];

    $form['trim_length'] = [
      '#title' => t('Trimmed limit'),
      '#type' => 'number',
      '#field_suffix' => t('characters'),
      '#default_value' => $this->getSetting('trim_length'),
      '#min' => 1,
      '#required' => TRUE,
    ];

    $form['limit'] = [
      '#type' => 'number',
      '#title' => t('Limit'),
      '#description' => t('Limit the number of paragraphs items to be returned. 0 to set no limit.'),
      '#min' => 0,
      '#default_value' => $this->getSetting('limit'),
      '#required' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $allowed_bundles = !empty($this->getAllowedBundles()) ? implode(', ', $this->getAllowedBundles()) : $this->t('All');
    $text_format = $this->getSetting('filter_format');
    $summary[] = $this->t('Allowed bundles - @allowed-bundles', ['@allowed-bundles' => $allowed_bundles]);
    $summary[] = $this->t('Limited to @limit Paragraphs.', ['@limit' => $this->getSetting('limit')]);
    $summary[] = $this->t('Trimmed limit: @trim_length characters.', ['@trim_length' => $this->getSetting('trim_length')]);
    $summary[] = $this->t('Paragraphs rendered as @mode.', ['@mode' => $this->getSetting('view_mode')]);
    if ($text_format) {
      $summary[] = $this->t('Filtered by the @text-format text format.', ['@text-format' => $this->getSetting('filter_format')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view_mode = $this->getSetting('view_mode');
    $limit = $this->getSetting('limit');
    $paragraphs = 0;
    $filter_format = $this->getSetting('filter_format');
    $filter_format = $filter_format ?: NULL;
    $allowed_bundles = $this->getAllowedBundles();
    $elements = [];
    $cacheKeyIds = [];
    $cacheTags = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      // Limit the summary to the allowed bundles. If none are set, allow all bundle types.
      if (!empty($allowed_bundles) && !in_array($entity->bundle(), $allowed_bundles)) {
        continue;
      }

      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
      $elements[$delta] = $view_builder->view($entity, $view_mode, $entity->language()->getId());

      $cacheKeyIds[] = $entity->id();
      $cacheTags = Cache::mergeTags($cacheTags, $entity->getCacheTags());

      $paragraphs++;

      // Break out of the loop if we have reached the maximum allowed paragraphs.
      if ($limit && $paragraphs >= $limit) {
        break;
      }
    }

    $markup = $this->getParagraphsSummary($elements, $filter_format, $this->getSetting('trim_length'));

    return [
      '#markup' => '<div class="paragraphs-summary">' . $markup . '</div>',
      '#cache' => [
        'keys' => ['entity_view', 'paragraphs_summary', $view_mode, implode(',', $cacheKeyIds)],
        'tags' => $cacheTags,
        'max-age' => Cache::PERMANENT,
      ],
    ];
  }

  /**
   * Creates a summary from Paragraph elements.
   *
   * @param array $elements
   *   An array of paragraph elements.
   * @param string|null $filter_format
   *   (optional) A filter format to use when creating the summary.
   * @param int|null $trim_length
   *   (optional) The trim length of the summary.
   *
   * @return string
   *    The generated summary.
   */
  protected function getParagraphsSummary(array $elements, $filter_format = NULL, $trim_length = NULL) {
    // Render all of the Paragraphs.
    $markup = \Drupal::service('renderer')->render($elements);

    // If a text format has been specified, filter the markup.
    if (isset($filter_format)) {
      $markup = check_markup($markup, $filter_format);
    }

    // Combining and filtering multiple paragraphs can result in extra spaces. Filter them out.
    $markup = trim(preg_replace('/\s\s+/', ' ', $markup));

    return text_summary($markup, $filter_format, $trim_length);
  }

  /**
   * Retrieves available Paragraph bundles.
   *
   * @return array
   *    The available Paragraph bundles.
   */
  protected function getParagraphBundles() {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($this->getFieldSetting('target_type'));
    $bundle_options = [];

    foreach ($bundles as $bundle_name => $bundle_info) {
      $bundle_options[$bundle_name] = $bundle_info['label'];
    }

    return $bundle_options;
  }

  /**
   * Retrieves available filter formats.
   *
   * @return array
   *    Available filter formats.
   */
  protected function getFilterFormats() {
    $filter_formats = [];

    foreach (filter_formats() as $key => $filter_format) {
      $filter_formats[$key] = $filter_format->get('name');
    }

    return $filter_formats;
  }

  /**
   * Retrieves the Paragraph bundles chosen in the formatter settings.
   *
   * @return array
   *    A filtered array of chosen Paragraph bundles.
   */
  protected function getAllowedBundles() {

    // Unchecked bundles have a value of 0. Remove those from the array.
    return array_filter($this->getSetting('allowed_bundles'), function ($v) {
      return $v !== 0;
    });
  }

}
